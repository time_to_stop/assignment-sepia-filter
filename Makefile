NAME := sepia

# change to file path of ASM implementation
ASM_DIR=transform
ASM_IMPLEMENTATION=$(ASM_DIR)/sepia.asm# No space after it

RM_FILE = rm
LOG_FILE=$(BUILDDIR)/log.log

##### Compiler / analyzer common configuration.

NASM = nasm -f elf64 

CC = clang
CFLAGS = -O3
LINKER = $(CC)

RM = rm -rf
MKDIR = mkdir -p

# Clang-tidy
CLANG_TIDY = clang-tidy

_noop =
_space = $(noop) $(noop)
_comma = ,

# Using `+=` to let user define their own checks in command line
CLANG_TIDY_CHECKS += $(strip $(file < clang-tidy-checks.txt))
CLANG_TIDY_ARGS = \
	-warnings-as-errors=* -header-filter="$(abspath $(INCDIR.main))/.*" \
	-checks="$(subst $(_space),$(_comma),$(CLANG_TIDY_CHECKS))" \

# Sanitizers
CFLAGS.none :=
CFLAGS.asan := -fsanitize=address
CFLAGS.lsan := -fsanitize=leak
CFLAGS.msan := -fsanitize=memory -fsanitize-memory-track-origins=2 -fno-omit-frame-pointer -fno-optimize-sibling-calls
CFLAGS.usan := -fsanitize=undefined

SANITIZER ?= none
ifeq ($(SANITIZER),)
override SANITIZER := none
endif

ifeq ($(words $(SANITIZER)),1)
ifeq ($(filter $(SANITIZER),all asan lsan usan msan none),)
$(error Please provide correct argument value for SANITIZER: all, asan, lsan, usan, msan or none)
endif
endif

# Using `+=` to let user define their own flags in command line
CFLAGS += $(CFLAGS.$(SANITIZER))
LDFLAGS += $(CFLAGS.$(SANITIZER))

ifeq ($(SANITIZER),none)
OBJDIR = obj
BUILDDIR = build
else
OBJDIR = obj/$(SANITIZER)
BUILDDIR = build/$(SANITIZER)
endif

##### Configuration for `main` target.

SOLUTION_DIR = solution

SRCDIR.main = $(SOLUTION_DIR)/src
INCDIR.main = $(SOLUTION_DIR)/include
OBJDIR.main = $(OBJDIR)/$(SOLUTION_DIR)

SOURCES.main += $(wildcard $(SRCDIR.main)/*.c) $(wildcard $(SRCDIR.main)/*/*.c)
TARGET.main  := $(BUILDDIR)/$(NAME)

##### Configuration for `tester` target.

TESTER_DIR = tester
TESTER_SCRIPT = $(TESTER_DIR)/test.sh

SRCDIR.tester = $(TESTER_DIR)/src
INCDIR.tester = $(TESTER_DIR)/include
OBJDIR.tester = $(OBJDIR)/$(TESTER_DIR)

SOURCES.tester += $(wildcard $(SRCDIR.tester)/*.c $(SRCDIR.tester)/*/*.c)
TARGET.tester  := $(BUILDDIR)/sepia-tester

CFLAGS.tester += $(strip $(file < $(TESTER_DIR)/compile_flags.txt)) $(CFLAGS) -I$(INCDIR.tester)

##### Rule templates. Should be instantiated with $(eval $(call template, ...))

# I use $$(var) in some variables to avoid variable expanding too early.
# I do not remember when $(var) is expanded in `define` rules, but $$(var)
# is expanded exactly at $(eval ...) call.

# Template for running submake on each SANITIZER value when SANITIZER=all is used. 
# $(1) - SANITIZER value (none/asan/lsan/msan/usan)
define make-sanitizer-rule

GOALS.$(1) := $$(patsubst %,%.$(1),$$(MAKECMDGOALS))

$$(MAKECMDGOALS): $$(GOALS.$(1))
.PHONY: $$(GOALS.$(1))

$$(GOALS.$(1)):
	@echo Running 'make $$(patsubst %.$(1),%,$$@)' with SANITIZER=$(1)
	@$(MAKE) $$(patsubst %.$(1),%,$$@) SANITIZER=$(1)

endef

# Template for compilation and linking rules + depfile generation and inclusion
# $(1) - target name (main/tester)
define make-compilation-rule

OBJECTS.$(1) := $$(SOURCES.$(1):$$(SRCDIR.$(1))/%.c=$$(OBJDIR.$(1))/%.o)
SRCDEPS.$(1) := $$(OBJECTS.$(1):%.o=%.o.d)

DIRS.$(1) := $$(sort $$(dir $$(OBJECTS.$(1)) $$(TARGET.$(1))))
DIRS += $$(DIRS.$(1))

.PHONY: build-$(1)

build-$(1): $$(TARGET.$(1))

$$(OBJDIR.$(1))/%.o: $$(SRCDIR.$(1))/%.c | $$(DIRS.$(1))
	$(CC) $(CFLAGS) -M -MP $$< >$$@.d
	$(CC) $(CFLAGS) -c $$< -o $$@

-include $$(SRCDEPS.$(1))

endef

# Template for testing rules.
# $(1) - directory with test
define make-test-rule

TST_NAME.$(1) := $$(notdir $(1))

.PHONY: $$(TST_OUTPUT.$(1)) test-$$(TST_NAME.$(1))
test: test-$$(TST_NAME.$(1))

test-$$(TST_NAME.$(1)): build-main build-tester
	$(TESTER_SCRIPT)  \
		--sepia $(TARGET.main) \
		--cmp $(TARGET.tester) \
		--test $(1) \
		--log $(LOG_FILE)

endef

##### Rules and targets.

.PHONY: all test clean check

ifeq ($(MAKECMDGOALS),)
MAKECMDGOALS := all
endif


ifeq ($(SANITIZER),all)
# Do all the work in sub-makes
$(foreach sanitizer,none asan lsan usan msan,$(eval $(call make-sanitizer-rule,$(sanitizer))))
else

all: build-main

check:
	$(CLANG_TIDY) $(CLANG_TIDY_ARGS) $(SOURCES.main)

$(foreach target,main tester,$(eval $(call make-compilation-rule,$(target))))
$(foreach test,$(sort $(wildcard $(TESTER_DIR)/tests/*)),$(eval $(call make-test-rule,$(test))))

$(OBJDIR.main)/$(ASM_IMPLEMENTATION).o : $(SRCDIR.main)/$(ASM_IMPLEMENTATION)
	$(MKDIR) $(OBJDIR.main)/$(ASM_DIR)
	$(NASM) -g $(SRCDIR.main)/$(ASM_IMPLEMENTATION) -o $(OBJDIR.main)/$(ASM_IMPLEMENTATION).o

$(TARGET.main): $(OBJDIR.main)/$(ASM_IMPLEMENTATION).o $(OBJECTS.main) | $(DIRS.main)
	$(LINKER) $(LDFLAGS) $(OBJECTS.main) $(OBJDIR.main)/$(ASM_IMPLEMENTATION).o -o $@

$(TARGET.tester): $(OBJECTS.tester) | $(DIRS.tester)
	$(RM_FILE) -f $(LOG_FILE)
	$(LINKER) $(LDFLAGS) $(OBJECTS.tester) -o $@

clean:
	$(RM) $(OBJDIR) $(BUILDDIR)

$(sort $(DIRS)):
	$(MKDIR) $@

endif
