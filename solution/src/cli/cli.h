#ifndef CLI_H
#define CLI_H

enum cli_state {
    CLI_USE_C_SEPIA_FILTER,
    CLI_USE_ASM_SEPIA_FILTER
};

struct cli_input {
    enum cli_state state;
    char* input_filename;
    char* output_filename;
};

struct cli_input get_cli_input(int argc, char* argv[]);


#endif // CLI_H