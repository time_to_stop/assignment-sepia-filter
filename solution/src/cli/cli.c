#include "cli.h"

#include <string.h>

#include "../utils/utils.h"

static const char* use_asm = "-asm";
static const char* usage = "Usage:\n %s <-asm> <input> <output>\n<-asm> - flag to use asm implementation of sepia (optional)\n";

struct cli_input get_cli_input(int argc, char* argv[]) {
    // args: program <maybe use asm flag> input output
    if (argc != 3 && argc != 4) {
        error(usage, argv[0]);
    }

    int arg_index = 1;
    struct cli_input input = { 0 };

    // first arg is program name
    input.state = CLI_USE_C_SEPIA_FILTER;

    // Check if asm flag
    if (strcmp(argv[arg_index], use_asm) == 0) {
        // If has asm flag, but only 3 args => no output file
        if (argc == 3) {
            error(usage, argv[0]);
        }

        input.state = CLI_USE_ASM_SEPIA_FILTER;
        arg_index++;
    }
    // If has no asm flag, but has 4 args
    else if (argc == 4) {
        error(usage, argv[0]);
    }

    input.input_filename = argv[arg_index++];
    input.output_filename = argv[arg_index++];

    return input;
}