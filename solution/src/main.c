#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/resource.h>
#include <sys/time.h>

#include <unistd.h>

#include "bmp/bmp.h"
#include "cli/cli.h"
#include "image/pixel.h"
#include "transform/sepia.h"
#include "utils/utils.h"

/* type of sepia implementation */
typedef void (*filter_t)(struct image*);

static filter_t filters[] = {
    [CLI_USE_C_SEPIA_FILTER]=&sepia_c, /* pointer to c impl */
    [CLI_USE_ASM_SEPIA_FILTER]=&sepia_asm /* poiner to asm impl */
};

static struct timeval get_current_time() {
    struct rusage r;
    getrusage(RUSAGE_SELF, &r);
    return r.ru_utime;
}

static long get_perfomance(struct timeval start) {
    const struct timeval end = get_current_time();

    return ((end.tv_sec - start.tv_sec) * 1000000L) +
        end.tv_usec - start.tv_usec;
}

int main(int argc, char* argv[]) {
    const struct cli_input input = get_cli_input(argc, argv);
    const filter_t sepia_filter = filters[input.state];

    FILE* input_bmp = fopen(input.input_filename, "rb");

    if (input_bmp == NULL) error("Unable to open file: %s\n", input.input_filename);

    FILE* output_bmp = fopen(input.output_filename, "wb");

    if (output_bmp ==  NULL) {
        fclose(input_bmp);
        error("Unable to open file: %s\n", input.output_filename);
    }

    struct image target;

    enum read_status read_code = from_bmp(input_bmp, &target);

    if (read_code != READ_OK) {
        destroy_image(&target);
        fclose(input_bmp);
        fclose(output_bmp);
        error("Read BMP failed:\n%s\n", read_error(read_code));
    }

    const struct timeval start = get_current_time();

    sepia_filter(&target);

    const long perfomance = get_perfomance(start);

    enum write_status write_code = to_bmp(output_bmp, &target);

    if (write_code != WRITE_OK) {
        destroy_image(&target);
        fclose(input_bmp);
        fclose(output_bmp);
        error("Write BMP failed:\n%s\n", write_error(write_code));
    }

    destroy_image(&target);
    fclose(input_bmp);
    fclose(output_bmp);

    // Print perfomance only when all ok
    printf("Time elapsed: %ld", perfomance);

    return 0;
}


