#include "sepia.h"

#include "../image/pixel.h"

#include <stdio.h>

extern void sepia_sse(uint64_t size, struct pixel* input);

static const float c[3][3] = {
    { .393f, .769f, .189f },
    { .349f, .686f, .168f },
    { .272f, .543f, .131f }
};

static unsigned char sat(uint64_t x) {
    return x < 256 ? x : 255;
}

static float to_float[256];

static void init_float() {
    for(int i = 0; i != sizeof(to_float)/sizeof(float); i++) {
        to_float[i] = (float)i;
    }
}

void sepia_one(struct pixel* target) {
    static int i = 1;

    const struct pixel temp = *target;

    // printf("Pixel %d:\nR(%f)=%f * %f + %f * %f + %f * %f = %f + %f + %f\n", i++, to_float[temp.r] * c[0][0] + to_float[temp.g] * c[0][1] + to_float[temp.b] * c[0][2],
    //     to_float[temp.r], c[0][0], to_float[temp.g], c[0][1], to_float[temp.b], c[0][2], to_float[temp.r] * c[2][0], to_float[temp.g] * c[2][1], to_float[temp.b] * c[2][2]);
    //        printf("G(%f)=%f * %f + %f * %f + %f * %f = %f + %f + %f\n", to_float[temp.r] * c[1][0] + to_float[temp.g] * c[1][1] + to_float[temp.b] * c[1][2],
    //     to_float[temp.r], c[1][0], to_float[temp.g], c[1][1], to_float[temp.b], c[1][2], to_float[temp.r] * c[2][0], to_float[temp.g] * c[2][1], to_float[temp.b] * c[2][2]);
    //        printf("B(%f)=%f * %f + %f * %f + %f * %f = %f + %f + %f\n", to_float[temp.r] * c[2][0] + to_float[temp.g] * c[2][1] + to_float[temp.b] * c[2][2],
    //     to_float[temp.r], c[2][0], to_float[temp.g], c[2][1], to_float[temp.b], c[2][2], to_float[temp.r] * c[2][0], to_float[temp.g] * c[2][1], to_float[temp.b] * c[2][2]);

    target->r = sat((uint64_t)(to_float[temp.r] * c[0][0] + to_float[temp.g] * c[0][1] + to_float[temp.b] * c[0][2]));
    target->g = sat((uint64_t)(to_float[temp.r] * c[1][0] + to_float[temp.g] * c[1][1] + to_float[temp.b] * c[1][2]));
    target->b = sat((uint64_t)(to_float[temp.r] * c[2][0] + to_float[temp.g] * c[2][1] + to_float[temp.b] * c[2][2]));
}

void sepia_c(struct image * target) {
    init_float();
  
    for(uint64_t i = 0; i != target->width; i++) {
        for (uint64_t j = 0; j != target->height; j++) {
            sepia_one(get_pixel(target, i, j));
        }
    }
}

void sepia_asm(struct image * target) {
    init_float();
    
    const uint64_t size = target->width * target->height;

    sepia_sse(size, target->data);

    for(int i = 0; i != size % 4; i++) {
        sepia_one(get_pixel(target, (size - i - 1) % target->width, (size - i - 1) / target->width));
    }
}
