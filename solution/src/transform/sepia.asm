global sepia_sse

section .data

; BLUE
align 16
c1: dd 0.131, 0.168, 0.189, 0.131
; GREEN
align 16
c2: dd 0.543, 0.686, 0.769, 0.543
; RED
align 16
c3: dd 0.272, 0.349, 0.393, 0.272


align 16
max: dd 255.0, 255.0, 255.0, 255.0

%macro STEP 1

    ; BLUE
    pxor xmm0, xmm0
    pinsrb xmm0, byte [rsp], 0
    pinsrb xmm0, byte [rsp + 3], 12
    cvtdq2ps xmm0, xmm0
    shufps xmm0, xmm0, %1 

    inc rsp

    ; GREEN
    pxor xmm1, xmm1
    pinsrb xmm1, byte [rsp], 0
    pinsrb xmm1, byte [rsp + 3], 12
    cvtdq2ps xmm1, xmm1
    shufps xmm1, xmm1, %1 

    inc rsp 

    ; RED
    pxor xmm2, xmm2
    pinsrb xmm2, byte [rsp], 0
    pinsrb xmm2, byte [rsp + 3], 12
    cvtdq2ps xmm2, xmm2
    shufps xmm2, xmm2, %1

    mulps xmm0, xmm3 
    mulps xmm1, xmm4    
    mulps xmm2, xmm5

    addps xmm0, xmm1
    addps xmm0, xmm2  

    pminsd xmm0, xmm6

    cvtps2dq xmm0, xmm0

    pextrb byte [rsi], xmm0, 0   ; extract bytes in rsi (output)
    pextrb byte [rsi + 1], xmm0, 4
    pextrb byte [rsi + 2], xmm0, 8
    pextrb byte [rsi + 3], xmm0, 12 

    inc rsp
    add rsi, 4

    shufps xmm3, xmm3, 0b_01_00_10_01
    shufps xmm4, xmm4, 0b_01_00_10_01
    shufps xmm5, xmm5, 0b_01_00_10_01

%endmacro

section .text

; rdi = size
; rsi = input
sepia_sse:
    movdqa xmm6, [rel max]

    movdqa xmm3, [rel c1]
    movdqa xmm4, [rel c2]
    movdqa xmm5, [rel c3]

.loop:
    cmp rdi, 4
    jl .end

    sub rsp, 12

    ; SAVE buffer
    mov rax, [rsi]
    mov [rsp], rax
    mov eax, [rsi + 8]
    mov [rsp + 8], eax

    STEP 0b_11_00_00_00
    STEP 0b_11_11_00_00
    STEP 0b_11_11_11_00

    add rsp, 3
    sub rdi, 4
    jmp .loop

.end:
    ret