#include <malloc.h>

#include "image.h"
#include "pixel.h"

bool create_image(struct image* image, uint64_t width, uint64_t height)
{
    if (image == NULL) return false;

    image->width = width;
    image->height = height;
    image->data = (struct pixel*) malloc(width * height * sizeof(struct pixel));

    return image->data != NULL ? true : false;
}

struct pixel* get_pixel(const struct image* image, uint64_t x, uint64_t y)
{
    return image->data + y * image->width + x;
}

void set_pixel(struct image* image, uint64_t x, uint64_t y, struct pixel* pixel)
{
    struct pixel* target = get_pixel(image, x, y);

    target->r = pixel->r;
    target->g = pixel->g;
    target->b = pixel->b;
}

void destroy_image(struct image* image)
{
    if (image != NULL)
    {
        free(image->data);
    }
}

