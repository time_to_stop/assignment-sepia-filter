#include "utils/utils.h"
#include "image/image.h"
#include "bmp/bmp.h"

static struct image read_bmp(const char* filename) {
    FILE* f = fopen(filename, "rb");

    if (f == NULL) {
        error("Failed to open file %s\n", filename);
    }

    struct image img; 
    enum read_status status = from_bmp(f, &img);

    if (status != READ_OK) {
        destroy_image(&img);
        fclose(f);
        error("BMP read failed\n%s\n", get_read_error(status));
    }

    fclose(f);
    return img;
}

int main(int argc, char* argv[]) {
    if (argc != 3) error("Usage:\n%s <first_bmp> <second_bmp>\n", argv[0]);

    struct image first = read_bmp(argv[1]), second = read_bmp(argv[2]);

    if (!soft_cmp_images(&first, &second)) error("Images is not equal\n");

    destroy_image(&first);
    destroy_image(&second);
    return 0;
}