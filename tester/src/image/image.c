#include "image.h"

#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>

#include "pixel.h"

bool create_image(struct image* image, uint64_t width, uint64_t height) {
    if (image == NULL) return false;

    image->width = width;
    image->height = height;
    image->data = (struct pixel*) malloc(width * height * sizeof(struct pixel));

    return image->data != NULL ? true : false;
}

struct pixel* get_pixel(const struct image* image, uint64_t x, uint64_t y) {
    return image->data + y * image->width + x;
}

void set_pixel(struct image* image, uint64_t x, uint64_t y, struct pixel* pixel) {
    struct pixel* target = get_pixel(image, x, y);

    target->r = pixel->r;
    target->g = pixel->g;
    target->b = pixel->b;
}

void destroy_image(struct image* image) {
    if (image != NULL) {
        free(image->data);
    }
}

bool soft_cmp_images(const struct image* left, const struct image* right) {
    if (left->width != right->width) return false;
    if (left->height != right->height) return false;

    for (uint64_t i = 0; i != left->width; i++) {
        for (uint64_t j = 0; j != left->height; j++) {
            struct pixel* left_pixel = get_pixel(left, i, j);
            struct pixel* right_pixel = get_pixel(right, i, j);

            
            // Allow wrong accuracy
            if (abs(left_pixel->r - right_pixel->r) > 1) return false;
            if (abs(left_pixel->g - right_pixel->g) > 1) return false;
            if (abs(left_pixel->b - right_pixel->b) > 1) return false;   
        }
    }

    return true;
}
