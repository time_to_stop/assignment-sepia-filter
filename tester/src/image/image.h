#ifndef IMAGE_H
#define IMAGE_H

#include <stdbool.h>
#include <stdint.h>

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

bool create_image(struct image* image, uint64_t width, uint64_t height);
struct pixel* get_pixel(const struct image* image, uint64_t x, uint64_t y);
void set_pixel(struct image* image, uint64_t x, uint64_t y, struct pixel* pixel);
void destroy_image(struct image* image);

bool soft_cmp_images(const struct image* left, const struct image* right);

#endif // IMAGE_H