#include "bmp.h"

#include "../image/pixel.h"

#define BMP_SIGNATURE 0x4D42
#define BMP_BIT_COUNT 24
#define BMP_SIZE 40
#define BMP_RESERVED_ZERO 0
#define ALIGN 3

static const char* read_errors[] = {
    [READ_OK]="BMP was read successfully",
    [READ_INVALID_SIGNATURE]="BMP invalid signature",
    [READ_INVALID_IMAGE_META]="BMP invalid image meta",
    [READ_INVALID_HEADER]="BMP invalid header",
    [READ_FAILED_TO_ALLOCATE_MEMORY]="BMP failed to allocate memory for image",
    [READ_INVALID_BITS]="BMP invalid image source"
};

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != BMP_BIT_COUNT) {
        return READ_INVALID_BITS;
    }

    if (!create_image(img, header.biWidth, header.biHeight)) {
        return READ_FAILED_TO_ALLOCATE_MEMORY;
    }

    for (uint64_t i = 0; i != header.biHeight; i++) {
        if (fread(get_pixel(img, 0, i), sizeof(struct pixel), header.biWidth, in) != header.biWidth) {
            return READ_INVALID_BITS;
        }

        if (header.biWidth % 4 != 0 && fseek(in, (4 - (int32_t)((header.biWidth * sizeof(struct pixel)) % 4)), SEEK_CUR) != 0) {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

const char* get_read_error(enum read_status status) {
    return read_errors[status];
}
