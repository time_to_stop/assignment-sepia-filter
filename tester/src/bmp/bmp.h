#ifndef BMP_H
#define BMP_H

#include <stdint.h>
#include <stdio.h>
#include <stdnoreturn.h>

#include "../image/image.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bFileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_IMAGE_META,
    READ_INVALID_HEADER,
    READ_FAILED_TO_ALLOCATE_MEMORY,
    READ_INVALID_BITS
};

enum read_status from_bmp(FILE* in, struct image* img);

const char* get_read_error(enum read_status status);

#endif //BMP_H