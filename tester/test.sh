#!/bin/bash

usage="test.sh --sepia <cmd> --cmp <cmd> --test <dir_with_test> --log <log_file>"

while [ $# -gt 0 ]; do
    key="$1"; shift

    case $key in
        --sepia)
            sepia="$1"; shift
            ;;
        --cmp)
            compare="$1"; shift
            ;;
        --test)
            test_dir="$1"; shift
            ;;
        --log)
            log="$1"; shift
            ;;
        -h|--help)
            echo $usage
            exit
            ;;
        *)
            echo "Unexpected argument"
            exit 1
            ;;
    esac
done

if [ -z "$sepia" ] || [ -z "$compare" ] || [ -z "$test_dir" ] || [ -z "$log" ]; then
    echo "Error: --sepia and --cmp and --test is required." 1>&2
    echo $usage 1>&2
    exit 1
fi

echo "Test $test_dir" >> $log

temp="$($sepia $test_dir/input.bmp $test_dir/output_c.bmp)"

if [ $? -ne 0 ]; then
    echo "$temp"
    exit 1
fi

echo "Using c impl: $temp" >> $log

temp="$($sepia -asm $test_dir/input.bmp $test_dir/output_asm.bmp)"

if [ $? -ne 0 ]; then
    echo "$temp"
    exit 1
fi

echo "Using asm impl: $temp" >> $log

temp="$($compare $test_dir/output_expected.bmp $test_dir/output_c.bmp)"
    
if [ $? -ne 0 ]; then
    echo "$temp"
    exit 1
fi
    
temp="$($compare $test_dir/output_expected.bmp $test_dir/output_asm.bmp)"

if [ $? -ne 0 ]; then
    echo "$temp"
    exit 1
fi

rm $test_dir/output_asm.bmp
rm $test_dir/output_c.bmp